import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, LoadingOptions, Slide, Slides, ToastController } from 'ionic-angular';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation';
import { User, Api } from '../../providers';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { ItineraryProvider } from '../../providers/itinerary/itinerary';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})


export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('itinerarySlide') slide: Slides;
  public map: any;
  public segmentContent:string = "map";

  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public modalCtrl: ModalController, 
              public loadingCtrl: LoadingController,
              public geolocation: Geolocation,
              public user: User,
              public api: Api,
              public transaction: TransactionProvider,
              public itineraryProvider: ItineraryProvider,
              public toastCtrl: ToastController
              // public alertCtrl: AlertController
              ) { 

                // this.isMapVisible = itineraryProvider.isTrackingMode;
 
              }

  ionViewDidLoad() {
    // console.table(this.user._user)
    // this.showLoading();
    this.loadMap();
  }

  showItineraryModal() {
    let modal = this.modalCtrl.create("ItineraryPage");
    modal.present();
  }

  showLoading() {
    let loadingOptions: LoadingOptions = {
      content: 'Loading Map...',
      duration: 1000
    };
    let loader = this.loadingCtrl.create(loadingOptions);
    loader.present();
  }

  viewItinerary(item: any) {
    this.navCtrl.push('IteneraryDetailsPage', {data: item});
  }

  loadMap() {
      // console.log('IM HERE!');
      this.itineraryProvider.isTrackingModeChange.subscribe((res: boolean) => {
        if(res) {
            console.log('Im Here! Loading the map');
            let mapOptions = null;
            let latLng = null;
            let myMarker = null;
            let targetPosition = null; 
            let myPos = { lat: 0, lng: 0 };
            
           
            this.trackMyPosition().subscribe(pos => {
              // checks if the passed position is the same as my current position
              // this means that i did not move
              if(pos.coords.latitude != myPos.lat && pos.coords.longitude != myPos.lng)  {
                latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

                // myPos = { lat:pos.coords.latitude, lng: pos.coords.longitude  };
                myPos.lat = pos.coords.latitude;
                myPos.lng = pos.coords.longitude;

                if(this.map == null) {
                  // creation of a new map
                  let placeIndex = 0;

                  mapOptions = {
                    center: latLng,
                    zoom: 18,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                  };
  
                  // Creating a new map
                  this.map = this.createMap(mapOptions);

                  // Put all target marker points
                  
                  this.itineraryProvider._currentItinerary.places.forEach(element => {
                    let temp = element.location.split(",");
                    element.lat = temp[0];
                    element.lng = temp[1];
                    placeIndex++;
                    let pos =  new google.maps.LatLng(temp[0],temp[1]);
                    element.marker = this.createMarker(this.map, pos,'',placeIndex.toString()); 
                    element.reached = false;
                  });
                
                  console.log('Creating a new map . . .');
                } else {
                  this.map.setCenter(latLng);
                  console.log('Updating the map . . .');
                }

                 // Creation of user marker
                if(myMarker == null) {
                  myMarker = this.createMarker(this.map, latLng, 'user');
                  console.log('Placing a new user marker . . .');
                } else {
                  // myMarker = null;
                  // myMarker = this.createMarker(this.map, latLng, 'user');
                  myMarker.setPosition(latLng);
                  console.log('Updating user marker . . .');
                }

                // Sending Data to server
                if(this.itineraryProvider._currentItinerary != null) {
                  let temp: any = {
                    trncode: this.itineraryProvider._currentItinerary.trncode,
                    user_id: this.user._user.id,
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude 
                  };
          
                  // send data to server 
                  this.api.post('position', temp )
                  .subscribe((res) => {
                    console.table(res);
                  });
                }

                // Distance Checking for the target position
                let target = this.itineraryProvider._currentItinerary.places[0];
                targetPosition = { lat: target.lat, lng: target.lng };
                targetPosition = new google.maps.LatLng(target.lat, target.lng);

                // Distance checking for each itinerary points
                let dist = google.maps.geometry.spherical.computeDistanceBetween(latLng, targetPosition);

                if(dist <= 35) {
                  console.log("Position Hit!");
                  let toast = this.toastCtrl.create({message: this.itineraryProvider._currentItinerary.places[0].name + ' is reached!', showCloseButton: true, closeButtonText: 'OK'});
                  this.itineraryProvider.changeTargetLocation();
                  toast.present();
                } else {
                  console.log("Position Not Hit!");
                }
    
              }else{
                console.log('NOT UPDATING MAP POSITION!');
              }   
            });      
        } else {
          console.log('Im Here! Hiding the map');
        }
      });      
  } 

  setMapOptions(latLng) {
   return {
      center: latLng,
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      // mapTypeControl: false,
      // zoomControl: false,
      //  streetViewControl: false
    }
  }

  trackMyPosition() {
    let gpsOptions: GeolocationOptions = { timeout: 20000, enableHighAccuracy: true };
    return this.geolocation.watchPosition(gpsOptions);
  }

  createMap(mapOptions) {
    return new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }

  createMarker(targetMap, latLng, type ?: string, lbl ?: string) {
    // creation of a marker
    let iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
    let marker = undefined;

    // checks if the map marker type is for user 
    if(type == 'user') {
      marker = new google.maps.Marker({
                  map: targetMap,
                  animation: google.maps.Animation.BOUNCE,
                  position: latLng,
                  icon: 'https://e-travel-bicol.000webhostapp.com/img/marker.png'
               });
    } else {
      marker = new google.maps.Marker({
        map: targetMap,
        // animation: google.maps.Animation.BOUNCE,
        position: latLng,
        label: lbl
        // icon: 'https://e-travel-bicol.000webhostapp.com/img/marker.png'
     });
    }

    return marker;
  }

  getLocationDistance(from: any, to: any) {
    return google.maps.geometry.spherical.computeDistanceBetween(from, to);
  }

  applyHaversine(locations){
      
      let usersLocation = {
          lat: 40.713744,
          lng: -74.009056
      };

      locations.map((location) => {

          let placeLocation = {
              lat: location.latitude,
              lng: location.longitude
          };

          location.distance = this.getDistanceBetweenPoints(
              usersLocation,
              placeLocation,
              'miles'
          ).toFixed(2);
      });

      return locations;
  }


  getDistanceBetweenPoints(start, end, units){
 
      let earthRadius = {
          miles: 3958.8,
          km: 6371
      };

      let R = earthRadius[units || 'miles'];
      let lat1 = start.lat;
      let lon1 = start.lng;
      let lat2 = end.lat;
      let lon2 = end.lng;

      let dLat = this.toRad((lat2 - lat1));
      let dLon = this.toRad((lon2 - lon1));
      let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      let d = R * c;

      return d;

  }

  toRad(x){
      return x * Math.PI / 180;
  }

  finishItinerary() {
    let loader = this.loadingCtrl.create({content: 'Saving . . .', duration: 3000})
    loader.present().then(() => {
      this.slide.slideTo(1,100);
      this.itineraryProvider.finishedItinerary();
    });
  }

  segmentChanged(event) {
    // console.log(event);

    // the segment is now showing the map
    if(this.segmentContent != 'map') {
      this.loadMap();
    }

    console.log('HERE');

  }

}
