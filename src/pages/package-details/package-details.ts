import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Cars, Places } from '../../providers';

/**
 * Generated class for the PackageDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-package-details',
  templateUrl: 'package-details.html',
})
export class PackageDetailsPage {

  public package: any;
  public cars: any = [];
  public places: any = [];
  public package_cars : any = [];
  public package_places: any = [];

  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams,
              public carProviders: Cars,
              public placesProviders: Places,
              public modalController: ModalController
              ) {
    this.package = this.navParams.get('data');

    this.carProviders.getCarList().subscribe((data: any) => {
      this.cars = data.cars;
      // console.log(this.cars);
      this.package.package_items.forEach((element: any) => {  
        if(element.car_id != null){ 
          this.cars.forEach((car: any) => {
            if(car.id == element.car_id) {
              this.package_cars.push(car);
            }
          });
        }

        ///  console.log(this.package_cars);
      });


    });

    this.placesProviders.getPlaces().subscribe((data: any) => {
      this.places = data.places;
      console.log(this.places);

      this.package.package_items.forEach((element: any) => {  
        if(element.place_id != null){ 
          this.places.forEach((place: any) => {
            if(place.id == element.place_id) {
              this.package_places.push(place);
            }
          });
        }

        // console.log(this.package_places);
      });
      
    });

    
    // console.log(this.package_cars);
    // console.log(this.package_places);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PackageDetailsPage');
  }

  showBookNowModal() {
    let saveModal = this.modalController.create("TransactionDetailsFormPage", { package: this.package });
    saveModal.present();
    this.navCtrl.pop();
  }

  showCarDetails(item: any) {
    this.navCtrl.push("CarDetailsPage",{item : item});
  }
}
