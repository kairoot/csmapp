import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController } from 'ionic-angular';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { DatePicker } from '@ionic-native/date-picker';
/**
 * Generated class for the TransactionDetailsFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transaction-details-form',
  templateUrl: 'transaction-details-form.html',
})
export class TransactionDetailsFormPage {

  public newTransaction:any = {
    dropOffPlace: "",
    dropOffTime: ""
  };

  public package: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewController: ViewController,
              public transactionProvider: TransactionProvider,
              public alertController: AlertController,
              public toastController: ToastController,
              public datePicker: DatePicker
             ) {

              this.package = this.navParams.get('package');
            
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionDetailsFormPage');
  }

  dismiss() {
    this.viewController.dismiss();
  }

  showDatePicker() {
    // this.datePicker.show({
    //   date: new Date(),
    //   mode: 'date',
    //   androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    // }).then( date => { 
    //   this.newTransaction.due_date = date
    //   let alert = this.alertController.create({ message: date.toString() });
    //   alert.present();
    // }).catch( err => console.error(err));

    return this.datePicker.show({
        date: new Date(),
        mode: 'time',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      });
  } 

  saveTransaction() {

    let time = new Date().getTime();
    let errorToast = this.toastController.create({ message: "Error! please fill up the drop off place field.", duration: 2000 });
    console.log('CURRENT TIME: ', time);
    
    // shows a time pickers
    if(this.newTransaction.dropOffPlace != '') {
      // Developers Purpose Only
      // adds a new transaction
      // this.transactionProvider.addTransaction(this.package.id, time, this.package.price, this.newTransaction.dropOffPlace,this.package.date_duration);

      this.showDatePicker().then(time => {

        this.newTransaction.dropOffTime = time;
    
        // this.transactionProvider.addNewTransaction(this.newTransaction.title, this.newTransaction.due_date);
  
        // adds a new transaction
        this.transactionProvider.addTransaction(this.package.id, time.valueOf(), this.package.price, this.newTransaction.dropOffPlace,this.package.date_duration);
      
        let alert = this.alertController.create({title: "Alert", message: "Transaction is now save in your transactions tab." });
    
        alert.present().then(() => {
          this.dismiss();
          // this.navCtrl.parent.select(3);
        });
  
      }).catch(err => console.error(err));
    } else {
      errorToast.present();
    }
     
  }
}
