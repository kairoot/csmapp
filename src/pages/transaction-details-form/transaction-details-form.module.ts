import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionDetailsFormPage } from './transaction-details-form';

@NgModule({
  declarations: [
    TransactionDetailsFormPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionDetailsFormPage),
  ],
})
export class TransactionDetailsFormPageModule {}
