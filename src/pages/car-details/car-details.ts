import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';


/**
 * Generated class for the CarDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-car-details',
  templateUrl: 'car-details.html',
})
export class CarDetailsPage {

  public car:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public itemProvider: ItemProvider
             ) {
    this.car = this.navParams.get("item");
    console.log(this.car);
  }


}
