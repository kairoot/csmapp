import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';

/**
 * Generated class for the DestinationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-destinations',
  templateUrl: 'destinations.html',
})
export class DestinationsPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public modalCtrl: ModalController,
              public loadingCtrl: LoadingController,
              public itemProvider: ItemProvider
             ) {

            } 

  ionViewDidLoad() { }

  showCartModal() {
    let modal = this.modalCtrl.create("CartPage");
    modal.present();
  }

  getPlaces() {

    // let loading = this.loadingCtrl.create({content: "Loading..."});
    // loading.present();

    // this.places.getPlaces()
    // .subscribe((res: any) => {
    //   this.placeList = res.places;
    //   console.table(this.placeList);
    //   console.log(res);
    //   loading.dismiss();
    // },err => {
    //   console.error(err);
    // });
  }

  viewPlace(place) {
    this.navCtrl.push('PlaceDetailsPage',{item: place});
  }

  addToCart() {
    console.log('Added to cart!');
  }

  showCars() {
    this.navCtrl.push("CarsPage");
  }

}
