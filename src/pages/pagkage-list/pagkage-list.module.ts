import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagkageListPage } from './pagkage-list';

@NgModule({
  declarations: [
    PagkageListPage,
  ],
  imports: [
    IonicPageModule.forChild(PagkageListPage),
  ],
})
export class PagkageListPageModule {}
