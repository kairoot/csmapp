import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { PackagesProvider } from '../../providers/packages/packages';

/**
 * Generated class for the PagkageListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pagkage-list',
  templateUrl: 'pagkage-list.html',
})
export class PagkageListPage {


  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public packageProvider: PackagesProvider,
              public alertController: AlertController,
              public modalController: ModalController
             ) {
    
      this.packageProvider.getPackages(this.navParams.get('date'));
  
  }



  ionViewDidLoad() {
    // console.log('ionViewDidLoad PagkageListPage');
  }

  viewPackage(item: any) {
    this.navCtrl.push("PackageDetailsPage", { data: item });
  }

  showBookNowModal() {
    let saveModal = this.modalController.create("TransactionDetailsFormPage");
    saveModal.present();
  }

}
