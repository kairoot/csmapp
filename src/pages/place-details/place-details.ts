import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';

@IonicPage()
@Component({
  selector: 'page-place-details',
  templateUrl: 'place-details.html',
})

export class PlaceDetailsPage {

  public place: any; 

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public itemProvider: ItemProvider
              ) {
    this.place = navParams.get("item");
    // console.table(this.place);
  }

  ionViewDidLoad() {
    
  }

}
