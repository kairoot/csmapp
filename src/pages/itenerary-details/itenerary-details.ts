import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the IteneraryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-itenerary-details',
  templateUrl: 'itenerary-details.html',
})
export class IteneraryDetailsPage {

  public item:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = this.navParams.get("data");
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad IteneraryDetailsPage');
  }

}
