import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IteneraryDetailsPage } from './itenerary-details';

@NgModule({
  declarations: [
    IteneraryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(IteneraryDetailsPage),
  ],
})
export class IteneraryDetailsPageModule {}
