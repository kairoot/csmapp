import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookNowModalPage } from './book-now-modal';

@NgModule({
  declarations: [
    BookNowModalPage,
  ],
  imports: [
    IonicPageModule.forChild(BookNowModalPage),
  ],
})
export class BookNowModalPageModule {}
