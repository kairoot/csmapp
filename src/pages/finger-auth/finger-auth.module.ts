import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FingerAuthPage } from './finger-auth';

@NgModule({
  declarations: [
    FingerAuthPage,
  ],
  imports: [
    IonicPageModule.forChild(FingerAuthPage),
  ],
})
export class FingerAuthPageModule {}
