import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio';

/**
 * Generated class for the FingerAuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finger-auth',
  templateUrl: 'finger-auth.html',
})
export class FingerAuthPage {

  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public fingerAuth: FingerprintAIO
             ) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad FingerAuthPage');
  }

  login() {
    let authOptions: FingerprintOptions = { clientId: 'Demo-Auth App', clientSecret: 'password' };
    this.fingerAuth.show(authOptions)
      .then(res => {
        console.log(res);
      })
      .catch(err => console.error(err));
  }

}
