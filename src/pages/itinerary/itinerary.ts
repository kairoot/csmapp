import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { ItineraryProvider } from '../../providers/itinerary/itinerary';
import { User, Api } from '../../providers';
/**
 * Generated class for the ItineraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-itinerary',
  templateUrl: 'itinerary.html',
})
export class ItineraryPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public user: User,
              public api: Api,
              public transaction: TransactionProvider,
              public itineraryProvider: ItineraryProvider, 
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController
             ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ItineraryPage');
  }

  viewItinerary(item: any) {
    this.navCtrl.push('IteneraryDetailsPage', {data: item});
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  finishItinerary() {
    let loader = this.loadingCtrl.create({content: 'Saving . . .', duration: 3000})
    loader.present().then(() => {
      // this.slide.slideTo(1,100);
      this.itineraryProvider.finishedItinerary();
    });
  }

}
