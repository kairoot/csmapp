import { Component, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, Tabs } from 'ionic-angular';

import { Tab1Root, Tab2Root, Tab3Root } from '../';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;
  // tab4Root: any = Tab4Root;
  // tab5Root: any = Tab5Root;

  tab1Title = "Home";
  tab2Title = "Cars";
  tab3Title = "Destinations";
  // tab4Title = "Transactions";
  // tab5Title = "Itinerary";
  

  @ViewChild('tab') tabRef: Tabs;

  constructor(public navCtrl: NavController, public translateService: TranslateService) {
    /*
        translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
          this.tab1Title = values['TAB1_TITLE'];
          this.tab2Title = values['TAB2_TITLE'];
          this.tab3Title = values['TAB3_TITLE'];
        });
    */
  }
}
