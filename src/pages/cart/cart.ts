import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';
import { TransactionProvider } from '../../providers/transaction/transaction';

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewCtrl: ViewController,
              public itemProvider: ItemProvider,
              public alertController: AlertController,
              public transactionProvider: TransactionProvider,
              public modalController: ModalController
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  showSaveAlert() {
    // let alert = this.alertController.create({
    //               title: 'Transaction',
    //               message: 'Enter the title of your transaction eg. Trip to bicol',
    //               inputs: [ {name : 'title', placeholder: 'Trip to bicol'} ], 
    //               buttons: [
    //                 { text: 'Cancel' },
    //                 { text: 'Submit', handler: data => this.saveTransaction(data.title) }
    //               ]
    //             });
    
    // alert.present();

    let saveModal = this.modalController.create("TransactionDetailsFormPage");

    saveModal.present();
  }

  saveTransaction(title) {
    this.transactionProvider.addNewTransaction(title);
    
    let alert = this.alertController.create({title: "Alert", message: "Transaction is now save in your transactions tab." });
    alert.present().then(() => {
      this.dismissModal();
      // this.navCtrl.parent.select(3);
    });
  }
}
