import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { ItineraryProvider } from '../../providers/itinerary/itinerary';

@IonicPage()
@Component({
  selector: 'page-transaction', 
  templateUrl: 'transaction.html',
})
export class TransactionPage {

  // public transactions:any[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public transactionProvider: TransactionProvider,
              public modalController: ModalController,
              public toastController: ToastController,
              public itinerary: ItineraryProvider
             ) {  

             }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TransactionPage');
    setInterval(() => {
      this.transactionProvider.reload();
      console.log('UPDATE IN 3 seconds interval');
    },3000);
    
  }

  viewTransaction(item: any) {
    // console.log(item);
    let modal = this.modalController.create("TransactionDetailPage" ,{ data: item });

    modal.onDidDismiss(data => {
      console.log(data);

      if(data.isCancelled == true) {
        this.navCtrl.parent.select(0);
      }
    });

    modal.present();
  }

  doRefresh(event) {

    let toast = this.toastController.create({message: "Your data is now up to date!",duration:1000});

    this.transactionProvider.loadServerData()
    .subscribe(res => {
        console.log('Finished reloading');
        toast.present();
        event.complete();
 
    },err => console.error('Error: ', err));
  } 
  
  startClick(item: any) {
    // Sets the map itinerary to be track.
    this.transactionProvider.setItineraryItem(item);
    this.navCtrl.parent.select(1);
  }

  checkIfDateIsPassed(date: any) {

    if(date != null) {
      let now = new Date();
      now.setHours(0,0,0,0);

      let due_date = new Date(date);

      console.log('DUE DATE :' + date);
      

      if(due_date < now) {
        console.log("IS PAST");
        
        return true;
      } 

      return false;
    }
  }

  checkIfDateIsNow(date: any) {

    // this function wil return true if the date is today 
    // else it will return false 
    let today = new Date().toLocaleDateString();

    // the date passed is today
    if(today == date) return true;
    else if(today > date) return false;
    else if(today < date) return false;
  }
}
