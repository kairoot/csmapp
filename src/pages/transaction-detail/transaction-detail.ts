import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { PackagesProvider } from '../../providers/packages/packages';

@IonicPage()
@Component({
  selector: 'page-transaction-detail',
  templateUrl: 'transaction-detail.html',
})
export class TransactionDetailPage {

  public transaction:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewController: ViewController ,
              public transactionProvider: TransactionProvider,
              public alertController: AlertController,
              public packageProvider: PackagesProvider
             ) {
    
    this.transaction = this.navParams.get("data");

    // this.transaction.due_date = new Date(this.transaction.due_date).toDateString();

    console.log(this.navParams.get("data"));
    // console.log('PASSED TRANSACTION : ', this.transaction);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TransactionDetailPage');
  }

  dismiss() {
    this.viewController.dismiss({  isCancelled: false });
  }

  cancelTransaction() {

    let alert = this.alertController.create({
      message: "Are you sure you want to cancel this transaction?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            this.transactionProvider.cancelTransaction(this.transaction.id);
            this.packageProvider.clearPackageData();
            this.transactionProvider.reload();  
            this.viewController.dismiss({  isCancelled: true });
          }
        },
        {
          text: "No"
        }
      ]
    });

    alert.present();
    
  }

  cancel() {

    // console.log('Transaction id :',this.transaction.id);
    // console.table(this.transaction);

    // this.transactionProvider.cancelTransaction(this.transaction.id);
    // this.packageProvider.clearPackageData();
    // this.transactionProvider.reload();
    // this.navCtrl.parent.select(0);
    // this.dismiss();
  }
}
