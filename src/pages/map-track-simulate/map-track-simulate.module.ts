import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapTrackSimulatePage } from './map-track-simulate';

@NgModule({
  declarations: [
    MapTrackSimulatePage,
  ],
  imports: [
    IonicPageModule.forChild(MapTrackSimulatePage),
  ],
})
export class MapTrackSimulatePageModule {}
