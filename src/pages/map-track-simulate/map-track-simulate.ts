import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



declare var google: any;
@IonicPage()
@Component({
  selector: 'page-map-track-simulate',
  templateUrl: 'map-track-simulate.html',
})

export class MapTrackSimulatePage {

  public myCurrentPosition = {
    name: "Current",
    lat: 13.1391,
    lng: 123.7438,
    position: undefined
  };

  public targetPositions = [ 
    {
      name: "Magayon Hotel",
      lat : 13.1511,
      lng : 123.7526,
      position: undefined
    }, 
    {
      name: "PNR",
      lat : 13.1515,
      lng : 123.7520,
      position: undefined
    },          
    {
      name: "Oro Site",
      lat: 13.1487,
      lng: 123.7525,
      position: undefined
    }             
  ];

  public index = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    
    this.track();

  }
  
  ionViewDidLoad() {
    
  }

  track() {
    // Distance Getting
    setInterval(() => {
      
      console.log("Here . . .");
      
      this.myCurrentPosition = this.targetPositions[this.index];

      let mypos = this.targetPositions[this.index];
      
      let start = {lat: mypos.lat, lng: mypos.lng};
      let end = { lat: this.targetPositions[this.index].lat, lng: this.targetPositions[this.index].lng};
      // console.log(mypos.position.lat,mypos.position.lng);
      
      var dist = this.getDistanceBetweenPoints(start,end,'miles').toFixed(2);
      
      // console.log(dist);

      if( parseFloat(dist) <= 0.02) {
        console.log("Place : " + this.targetPositions[this.index].name + " is reached!" + " DISTANCE: " + dist);
        this.index++;
      }  

    },2000);


  }

  


  applyHaversine(locations){
      
      let usersLocation = {
          lat: 40.713744,
          lng: -74.009056
      };

      locations.map((location) => {

          let placeLocation = {
              lat: location.latitude,
              lng: location.longitude
          };

          location.distance = this.getDistanceBetweenPoints(
              usersLocation,
              placeLocation,
              'miles'
          ).toFixed(2);
      });

      return locations;
  }

  getDistanceBetweenPoints(start, end, units){
 
      let earthRadius = {
          miles: 3958.8,
          km: 6371
      };

      let R = earthRadius[units || 'miles'];
      let lat1 = start.lat;
      let lon1 = start.lng;
      let lat2 = end.lat;
      let lon2 = end.lng;

      let dLat = this.toRad((lat2 - lat1));
      let dLon = this.toRad((lon2 - lon1));
      let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      let d = R * c;

      return d;
  }

  toRad(x){
    return x * Math.PI / 180;
  } 

}
