import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker';
import { PackagesProvider } from '../../providers/packages/packages';
import { TransactionProvider } from '../../providers/transaction/transaction';

/**
 * Generated class for the PackagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@IonicPage()
@Component({
  selector: 'page-packages',
  templateUrl: 'packages.html',
})
export class PackagesPage {

  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams,
              public dateTimePicker: DatePicker,
              public packageProvider: PackagesProvider,
              public modalController: ModalController,
              public transactionProvider: TransactionProvider
             ) {
              // this.packageProvider.getPackages(0);
             
  }

  ionViewDidLoad() {
    
  }

  showDatePicker() { 

    let newDate = new Date();
    let mil = newDate.getTime();

    let dateTimePickerOptions: DatePickerOptions = {
      date: new Date(),
      mode: 'date',
      allowOldDates: false,
      minDate: Date.now(),
      androidTheme: this.dateTimePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }
    
    // For development purpose
    // this.packageProvider.getPackages(mil);

    // Shows a date time picker
    this.dateTimePicker.show(dateTimePickerOptions)
        .then((date) => {
          console.log('GOT DATE : ',date);

          // this.navCtrl.push("PagkageListPage",{ date: date.getTime() });
          this.packageProvider.getPackages(date.getTime());

        })
        .catch((err) => console.error('Error occurred while getting date: ',err));
  } 

  viewPackage(item: any) {
    this.navCtrl.push("PackageDetailsPage", { data: item });
  }

  showBookNowModal() {
    let saveModal = this.modalController.create("TransactionDetailsFormPage");
    saveModal.present();
  }

  convertDate(mil: any) {
    return new Date(mil).toDateString();
  }

}
