// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'LoginPage';
// export const FirstRunPage = 'TabsPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'PackagesPage';
export const Tab2Root = 'HomePage';
export const Tab3Root = 'TransactionPage';
// export const Tab4Root = 'TransactionPage';
// export const Tab5Root = 'ItineraryPage';
// export const Tab5Root = 'CartPage';
 