import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarsPage } from './cars';
// import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    CarsPage,
  ],
  imports: [
    IonicPageModule.forChild(CarsPage),
    // IonicImageLoader
  ],
})
export class CarsPageModule {}
