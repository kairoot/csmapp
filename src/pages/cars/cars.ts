import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { Cars, Cart } from '../../providers';
import { Car } from '../../models/car';
// import { ImageLoaderConfig } from 'ionic-image-loader';
import { ItemProvider } from '../../providers/item/item';



/**
 * Generated class for the CarsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cars',
  templateUrl: 'cars.html',
})
export class CarsPage {

  // public carList: any[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public modalCtrl: ModalController,
              public cars: Cars,
              public loadingCtrl: LoadingController,
              public cart: Cart,
              public itemProvider: ItemProvider,
             // private imageLoaderConfig: ImageLoaderConfig
              ) {

                // this.getCars();
                // this.imageLoaderConfig.enableDebugMode();
                // this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
                // this.imageLoaderConfig.setFallbackUrl('assets/imgs/default.jpg');
                // this.imageLoaderConfig.setMaximumCacheAge(24 * 60 * 60 * 1000);
              }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad CarsPage');
  }


  showCartModal() {
    let modal = this.modalCtrl.create("CartPage");
    modal.present();
  }

  getCars() {
    // let loader = this.loadingCtrl.create({content: 'Please wait...'});
    // loader.present();
    // this.cars.getCarList()    
    // .subscribe((res: any) => {
    //     this.carList = res.cars;
    //     console.table(this.carList);
    //     // console.log(res);
    //     loader.dismiss();
    // },err => {
    //   console.error('ERROR : ',err);
    // });
  }

  openCarDetails(item: any) {
    this.navCtrl.push('CarDetailsPage',{item: item});
  } 

  addToCart(index) {
    // let item = this.carList[index];
    // this.cart.addItem(item, 'cars', index);
    // this.carList[index].selected = true;

    // console.log("CARS");
    // console.table(this.carList[index]);
  }

  onImageLoad(event) {
    console.log('Image Ready: ',event);
  }
}
