export class Car {
    id: number;
    name: string;
    price: number;
    brand: string;
    description: string;
    plate_number: string;
    seat_count: number;
    door_count: number;
    is_air_conditioned: boolean;
    engine_type: string;
    is_available: boolean;
    picture: string;
    created: string;
    modified: string;

    constructor(values: Object = {}) {
        Object.assign(this,values);
    }

}
