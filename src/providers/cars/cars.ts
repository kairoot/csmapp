import { Injectable } from '@angular/core';
import { Api } from '../api/api';

@Injectable()
export class Cars {

  constructor(public api: Api) { }

  getCarList() {
    
    let req = this.api.get('cars');

    return req;
    // req.subscribe((res: any) => {
    //   console.log(res);
    // });
  }

}
