import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { StorageProvider } from '../storage/storage';
import { Api } from '../api/api';
import { TransactionProvider } from '../transaction/transaction';



@Injectable()
export class ItineraryProvider {


  isTrackingMode: boolean = false;

  _currentItinerary: any;

  _finishedPlaceList: any = [];
 
  _itineraryLogs:any = [];
  
  isTrackingModeChange: Subject<boolean> = new Subject<boolean>();

  constructor(
              public api: Api , 
              public storage: StorageProvider,
              // public transaction: TransactionProvider
             ) {
    this.isTrackingModeChange.subscribe((val) => {
      this.isTrackingMode = val;
    });

    this.storage.getStorageItem('recent_itinerary')
    .then((res) => { 
      if(res != null) {
        this._itineraryLogs = res; 
        console.log('SAVED ITINERARIES :', res);
      }
    })
    .catch(err => console.error('ERROR', err));
  }

  toggleIsTrackingMode() {
    this.isTrackingModeChange.next(!this.isTrackingMode);
  }

  // getIsTrackingMode() {
  //   this.isTrackingModeChange.next(this.isTrackingMode);
  // }

  getItinerary(){
    // return  new Promise(resolve => this.itinerary);
  }

  changeTargetLocation() {
    if(this._currentItinerary.places.length > 0) {
      let temp = this._currentItinerary.places[0];
      this._currentItinerary.places.splice(0, 1);
      temp.reached = true;
      temp.marker = null;
      // this._currentItinerary.places.push(temp);
      this._finishedPlaceList.push(temp);
    } else {
      this._currentItinerary.places = []; // Reinitialized
    }
  }

  finishedItinerary() {
    this.isTrackingModeChange.next(false);     
    this._currentItinerary.places = this._finishedPlaceList;;

    this.api.post('transaction/finish',{ trncode: this._currentItinerary.trncode })
    .subscribe((data:any) => {
      console.log(data); 

      if(data.status == 'SUCCESS') {
        // this._itineraryLogs.push(this._currentItinerary);
        this._itineraryLogs.unshift(this._currentItinerary);

        this._currentItinerary = null;
        this._finishedPlaceList = [];
        
        this.storage.setStorageItem("recent_itinerary", this._itineraryLogs)
        .then(res => console.log(res))
        .catch(err => console.error(err));
      }
    });    
  }

  public clearItineraryData() {
    this._currentItinerary = null;
    this._finishedPlaceList = [];
    this._itineraryLogs = null;
    this.isTrackingMode = false;
  }

} 
