import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageProvider {

  constructor(public storage: Storage) { }

  getStorageItem(key: string) {
    // return this.storage.getItem(key);
    return this.storage.get(key);
  }

  setStorageItem(key?: string, val ?: any) {
    return this.storage.set(key, val);
  }

  clearStorage() {
    return this.storage.clear();
  }
}
