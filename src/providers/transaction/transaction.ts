import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, ViewChild } from '@angular/core';
import { ItemProvider } from '../item/item';
import { StorageProvider } from '../storage/storage';
import { Api } from '../api/api';
import { User } from '../user/user';
import { ItineraryProvider } from '../itinerary/itinerary';
import { LoadingController } from 'ionic-angular';
import { PackagesProvider } from '../packages/packages';

@Injectable()
export class TransactionProvider {
 

  public transactions:any[] = [];
  public dates:any[] = [];
  public transactionDataFromServer:any = [];
  public target:any;

  constructor(
              public itemProvider: ItemProvider,
              public storage: StorageProvider,
              public api: Api,
              public user: User,
              public itineraryProvider: ItineraryProvider,
              public loadingCtrl: LoadingController,
              public packageProvider: PackagesProvider,
              // public navCtrl: NavController

             ) {
      // this.initializeTransaction();
      this.loadServerData();
      // storage.clearStorage().then(res => console.log(res));
  }

  initializeTransaction() {
    this.storage.getStorageItem("transactions")
      .then((result) => {
        if(result)
          this.transactions = result;     
          
      }).catch((err) => {
        console.error('Error:', err);
      });
  }

  addTransaction(package_id: any, dropOffTime: any, total: any, dropOffPlace: any, date_duration: any) {


    // Generation of unique id
    let date = new Date();

    let daysToBeAdded = date_duration - 1;

    let end_date = new Date( this.packageProvider.target_date );

    end_date.setDate(end_date.getDate() + daysToBeAdded);


    // console.log('TARGET DATE: ',end_date);
    // console.log('TARGET DATE MIL: ',end_date.valueOf());

    // initialization of a new transaction 
    let transData = { 
                      package_id: package_id, 
                      drop_off_time: dropOffTime, 
                      status: 'PENDING' , 
                      trncode: date.valueOf(),  
                      user_id: this.user._user.id,
                      total: total,
                      date: this.packageProvider.target_date,
                      drop_off_place: dropOffPlace,
                      due_date: end_date.valueOf()
                    };

    console.log('TRANS_DATA : ', transData);
    
    // Send data to server
    this.sendTransactionToServer(transData);    
    
  }

  addNewTransaction(title?: string, dueDate?: Date, dropOffTime ?: number) {
    let transData:any = { cars: this.itemProvider._cart.cars, places: this.itemProvider._cart.places };
    let date = new Date();
    let newDueDate = (dueDate.getMonth() + 1) + '-' + dueDate.getDate() + '-' + dueDate.getFullYear();
    // let date = "10-29-2018";
    // setup
    transData.title = title;
    transData.total = this.itemProvider.getTotalPayment();
    transData.status = "PENDING";
    // Get something ID 
    transData.trncode = date.valueOf();
    transData.duedate  = newDueDate;
    
    let currentDate =  (date.getMonth() + 1)  +"-" + date.getDate() + "-" + date.getFullYear();

    let temp:any = this.checkIfKeyExist(currentDate);
    
    let transaction = null;

    // saving to storage
    if(!temp.exist) {
      transaction = {
        date: currentDate, 
        data: [transData] 
      }
      this.transactions.push(transaction);
    } else {
      this.transactions[temp.index].data.push(transData);
    } 
    transData.date = currentDate;
    transData.user_id = this.user._user.id;
    this.saveToStorage('transactions', this.transactions);
    this.sendTransactionToServer(transData);
    console.log("New Transaction Data: ",transData);
    this.itemProvider.clearCart();
  } 

  checkIfKeyExist(key ?: string) {
    let result:any = { exist: false, index: -1 }; 
    
    this.transactions.forEach((data,i) => {
      // console.log(data);
      if(data.date === key) {
        console.log('Here!');
        result.exist = true;
        result.index = i;
      }
    });
    return result;
  }

  saveToStorage(key:string, val: any) {
    this.storage.setStorageItem(key, val).then((result) => {
      console.log(result);
      console.log('SUCCESS IN SAVING . . .');
    }).catch((err) => { 
      console.error('ERROR', err);
    });
  }

  sendTransactionToServer(transaction: any) {
    this.api.post('transaction', transaction)
      .subscribe((res) => {
        // console.table(res);
        console.log('ADD NEW TRANSACTION RESULT : ', res);
        this.packageProvider.getPackages(this.packageProvider.target_date);
      },err => console.error(err));
  }

  loadServerData() {
    let req = this.api.get('transactions/' + this.user._user.id).share();
    
    console.log(req);
    req.subscribe( (res:any) => {

      console.log(res);
      let trans:any[];        
      trans = res.data;

      trans.forEach(element => {
          // console.log(element.date);
          
         element.date = new Date( parseFloat(element.date)).toLocaleDateString();
         element.due_date = new Date( parseFloat(element.due_date)).toLocaleDateString();
         // element.drox`p_off_time = this.msToTime(element.drop_off_time);

         let time = new Date(parseFloat(element.drop_off_time));

         element.drop_off_time =  time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

         // console.log(element.date);
         
      });
      
      // this.dates = [];

      // let final:any = [];

      // let transactionData = { 
      //   date: "",
      //   data: []   
      // }

      // trans.forEach(data => {
      //   if( this.dates.indexOf(data.date) == -1) 
      //     this.dates.push(data.date);
      // });

      // this.dates.forEach(date => {
      //   let x = trans.filter((data) => {
      //     return data.date == date;
      //   });  
      //   transactionData.date = date;
      //   transactionData.data = x;
      //   final.push(transactionData);
      //   transactionData = { date: "", data: [] };
      // });

      console.log("Transactions : ", trans);

      /// this.transactions = final;
      this.transactions = trans;

    },err => console.error(err));

    return req;
  }

  reload() {
    this.loadServerData().subscribe((res:any) => {},err => console.error(err));
  }

  setItineraryItem(item: any) {
    // this.target = item;

    let loader = this.loadingCtrl.create({content: "Loading . . .",duration:3000});
    loader.present().then(() => {
      this.itineraryProvider._currentItinerary = item;
      this.itineraryProvider.toggleIsTrackingMode();
      this.target = item;
      // this.navCtrl.parent.select(0);
      console.log('Target: ', this.itineraryProvider._currentItinerary);

    });
  }

  public clearTransactionData() {
    this.transactions = null;
  }

  public cancelTransaction(id: any) {
    this.api.get('transaction/cancel/' + id).subscribe((data) => {
      console.log(data);
      this.packageProvider.loadData();
      // this.navCtrl.parent.select(0);
    })
  }
  
}
