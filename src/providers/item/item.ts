import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Places } from '../places/places';
import { Cars } from '../cars/cars';

@Injectable()
export class ItemProvider {

  public _cars:any[] = [];
  public _places:any[] = [];
  public _cart:any = { cars:[], places:[] };

  constructor(public http: HttpClient,
              public cars: Cars,
              public places: Places
              ) {
                
      this.clearCart();

      // Getting of cars 
      this.cars.getCarList()    
      .subscribe((res: any) => {
          this._cars = res.cars;
          console.table(this._cars);
      },err => {
        console.error('ERROR : ',err);
      });

      // Getting of places
      this.places.getPlaces()
      .subscribe((res: any) => {
        this._places = res.places;

        this._places.forEach(element => {
          let temp = element.location.split(",");
          element.lat = temp[0];
          element.lng = temp[1];
        });

        console.table(this._places);
      },err => {
        console.error(err);
      });
  }
  
  addToCart(item: any, target: string) {
    if(target == "cars") {
      let index = this._cars.indexOf(item);
      this._cars[index].selected = true;
      this._cart.cars.push(item);
    } else {
      let index = this._places.indexOf(item);
      this._places[index].selected = true;
      this._cart.places.push(item);
    }
  }

  cancelItemInCart(item: any, target: string, index?: number) {
    if(target == "cars") {
      let cartIndex = this._cart.cars.indexOf(item);
      let carIndex = this._cars.indexOf(item);
      this._cart.cars.splice(cartIndex, 1);
      this._cars[carIndex].selected = false;
    } else {
      let cartIndex = this._cart.places.indexOf(item);
      let placeIndex = this._places.indexOf(item);
      this._cart.places.splice(cartIndex, 1);
      this._places[placeIndex].selected = false;
    }
  }

  getTotalPayment() {
    let carsPayment = 0;
    let placesPayment = 0;
    let total = 0;

    this._cart.cars.forEach(car => {
       carsPayment += car.price; 
    });

    this._cart.places.forEach(place => {
      placesPayment += place.price;
    });

    total = carsPayment + placesPayment;
    console.log('Total : ', total);
    return total;
  }

  clearCart() {
    this._cart.cars = [];
    this._cart.places = [];
    this._cars.forEach(car => car.selected = false);
    this._places.forEach(place => place.selected = false);
  }
  
}
