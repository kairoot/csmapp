import { Injectable } from '@angular/core';

@Injectable()
export class Cart {

  public _cars:any = [];
  public _places:any = [];

  constructor( ) { }

  addItem(item: any, target?: string, index?: number) {

    item.selected = true;
    item.index = index;

    if(target == 'cars') {
      this._cars.push(item);
      console.log('Added to CART:Car ', item);
      console.table(this._cars);
    } else {
      this._places.push(item);
      console.log('Added To Places: ', item);
      console.table(this._places);
    }
  }

}
