import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ItemProvider } from '../item/item';
import { StorageProvider } from '../storage/storage';
import { Api } from '../api/api';
import { User } from '../user/user';
import { LoadingController } from 'ionic-angular';

 

@Injectable()
export class PackagesProvider {

  public packages:any = null;

  public target_date: any = null;

  public is_updating = false;
    
  constructor(
               public http: HttpClient, 
               public api: Api,
               public loadingCtrl: LoadingController
              ) {
      
      // if(this.target_date != null)
      // setInterval(this.getPackages, 3000);
      // this.updatePackages();
  }

  public getPackages(date: any) {

    // Initialize the target_date 
    this.target_date = date;

    this.loadData();
  }

  public loadData() {
    // loads all packages from the server

    if(this.target_date == null) return; 

    this.api.get('package/' + this.target_date)
      .subscribe((data: any) => {
        console.log('PACKAGES : ' , data);
        this.packages = data.packages;      
      });
  }

  public load() {
    if(this.target_date == null) return; 

    this.api.get('package/' + this.target_date)
      .subscribe((data: any) => {
        console.log('HERE AUTO UPDATE');
        console.log('PACKAGES : ' , data);
        this.packages = data.packages;      
      });
  }

  public updatePackages() {
    this.is_updating = true;
    setInterval(this.load, 3000);
  }

  public clearPackageData() {
    this.target_date = null;
    this.packages = null;
  }


}
